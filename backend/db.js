const mongoose = require('mongoose');
const colors = require('colors');

const database = {
    initialize: ()=> {
        try {
            const db = mongoose.connect(process.env.MONGO_URI, {useNewUrlParser: true, useUnifiedTopology: true});
            db.then(() => console.log('Connected to the database 👌'.blue))
        } catch (error) {
            throw error;
        }
    }
}

module.exports = database;