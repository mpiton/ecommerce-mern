const express = require('express');
const morgan = require('morgan');
const colors = require('colors')
const {graphqlHTTP} = require('express-graphql')
const path = require('path');

//Schema GraphQL
const schema = require('./schemas/index');



//instance express
const app = express();
const port = process.env.PORT || 4000;
const cors = require('cors');
const database = require('./db');
require('dotenv').config()


//Models
const Product = require('./models/product');

//API RESTful
app.get("/products", async (req, res) => {
    const products = await Product.find({});
    try {
        res.send(products);
    } catch (error) {
        res.status(500).send(error);
    }
})
app.get('/products/:category', async (req, res) => {
    const category = req.params.category
    const products = await Product.find({ category : category})
    try {
        res.send(products)
    } catch(e) {
        res.status(500).send(err)
    }
})

//middleware
app.use(morgan("tiny"));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET");
    res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
    });
    app.options("*", cors());

//GraphQL UI
app.use(
    '/graphql',
    graphqlHTTP({
        schema: schema,
        graphiql: true,
    }),
);

/*Adds the react production build to serve react requests*/
app.use(express.static(path.join(__dirname, "../frontend/build")));
/*React root*/
app.get("*", (req, res) => {
res.sendFile(path.join(__dirname + "../frontend/build/index.html"));
});
app.listen(port, () => {
    console.log(`Server working on port ${port} 👌`.magenta);
    database.initialize();
});