import {useSelector, useDispatch} from "react-redux"
import React, { useEffect, useMemo, useState } from 'react';
import {updateUserProfile} from "../../lib/redux/reducers/user"

const styles =  {
    valid : {
        display: 'none'
    },
    errors: {
        color: "#BF344D",
        display: 'block'
    }
}
function Checkout({history}) {
    const {current} = useSelector(state => state.user)
    const dispatch = useDispatch()
    const [clientDetails, setClientDetails] = useState({
        givenName: current?.givenName,
        familyName: current?.familyName,
        email: current?.email
    })
    const [required, setRequired] = useState({
        givenName: false,
        familyName: false,
        email: false
    })

    useEffect(() => {
        setClientDetails({
            givenName: current?.givenName,
            familyName: current?.familyName,
            email: current?.email
        })
    }, [current?.email, current?.familyName, current?.givenName])

    useEffect(() => {
        const patternEmail = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        const patternName = new RegExp(/^[a-zA-Zçéèëêîïä\- ]*$/)
        setRequired({
            givenName: (clientDetails?.givenName?.length !== 0) && (patternName.test(clientDetails.givenName)),
            familyName: (clientDetails?.familyName?.length !== 0) && (patternName.test(clientDetails.familyName)),
            email: (clientDetails?.email?.length !== 0) && (patternEmail.test(clientDetails.email))
        })
    },[clientDetails.email, clientDetails?.familyName, clientDetails?.givenName, required.email])

    const isValid = useMemo(()=> {
        let errors = []
        Object.values(required).map((value) => {
            if (!value) {
                errors.push(value)
            }
            return errors;
        })
        return !errors.length
    }, [required])
    const handleOnChange = (event) => setClientDetails(prevState => ({
        ...prevState,
        [event.target.name]: event.target.value
    }))

    const handleOnSubmit = (event) => {
        event.preventDefault();
        dispatch(updateUserProfile(clientDetails))
        history.push("/payment")
    }
    return (
        <section className="pt-5 pb-5">
            <div className="container">
                <div className="py-5 text-center row justify-content-center">
                    <div className="col-md-10">
                        <h2>Client Details :</h2>
                    </div>
                </div>
                <div className="row justify-content-center rounded shadow pt-5 pb-5 bg-white ">
                    <div className="col-md-8 ">
                        <form className="needs-validation" onSubmit={handleOnSubmit}>
                            <div className="row">
                                <div className="col-md-6 mb-3">
                                    <label htmlFor="firstName">First name</label>
                                    <input
                                    className="form-control"
                                    type="text"
                                    name="givenName"
                                    id="firstName"
                                    placeholder="please enter first name"
                                    value={clientDetails.givenName}
                                    onChange={handleOnChange}
                                    />
                                    <small style={required.givenName ? styles.valid : styles.errors}>
                                    Valid first name is required.
                                    </small>
                                </div>
                                <div className="col-md-6 mb-3">
                                    <label htmlFor="lastName">Last name</label>
                                    <input
                                    className="form-control"
                                    type="text"
                                    name="familyName"
                                    id="lastName"
                                    placeholder="please enter last name"
                                    value={clientDetails.familyName}
                                    onChange={handleOnChange}
                                    />
                                    <small style={required.familyName ? styles.valid : styles.errors}>
                                    Valid last name is required.
                                    </small>
                                </div>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="email">Email </label>
                                <input
                                className="form-control"
                                type="email"
                                name="email"
                                id="email"
                                placeholder="you@example.com"
                                value={clientDetails.email}
                                onChange={handleOnChange}
                                />
                                <small style={required.email ? styles.valid : styles.errors}>
                                    Please enter a valid email address for order updates
                                </small>
                            </div>
                            <button className="btn btn-orange btn-lg btn-block" type="submit" disabled={!isValid}>
                                Continue to checkout
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    );
}
export default Checkout;