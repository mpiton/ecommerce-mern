import loader from "../../loader.svg";

const Alert = {
  Error: ({ status }) => {
    return (
      status && (
        <div class="alert bg-redlight is-white" role="alert">
          Transaction could not be completed - please try again
          <i class="fas fa-times"></i>
        </div>
      )
    );
  },
  Cancelled: ({ status }) => {
    return (
      status && (
        <div class="alert bg-yellow" role="alert">
          Transaction cancelled
        </div>
      )
    );
  },
  Confirmed: ({ status }) => {
    return (
      status && (
        <div class="alert bg-orangelight" role="alert">
          Thank you for your order ❤️ - You will be redirected in a few seconds
          ...
          &nbsp; <img src={loader} className="App-logo ml-4 is-orangelight" alt="loader" />
        </div>
      )
    );
  },
};
export default Alert;
