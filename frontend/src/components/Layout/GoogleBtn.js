import React from "react";
import { useState } from "react";
import {GoogleLogout, GoogleLogin} from "react-google-login";
import {handleLogin, handleLogout, handleErrors} from "../../lib/redux/reducers/user"
import { useSelector, useDispatch} from 'react-redux';

const styles = {
  img: {
    borderRadius: "50%",
    width: "32px",
    height: "32px",
    border: "2px solid #bdc3c7",
  },
  dropdown: {
    background: "transparent",
    borderColor: "transparent",
  },
};

const GoogleBtn = () => {
  const CLIENT_ID = "145720897690-1umjd5enqt337mg0ke1f3n4v0si8kblp.apps.googleusercontent.com";
  const dispatch = useDispatch()
  const {current, error} = useSelector(state => state.user)

  const handleLoginSuccess = (res) => dispatch(handleLogin(res.profileObj))
  const handleLogoutSuccess = () => dispatch(handleLogout())
  const handleLoginFailure = (res) => dispatch(handleErrors({...res.error, ...res.details}))
  const handleLogoutFailure = (res) => dispatch(handleErrors({...res.error, ...res.details}))
  return (
    <>
      {current ? (
        <>
          <div class="dropdown">
            <button
              class="btn btn-secondary dropdown-toggle"
              type="button"
              id="dropdownMenuButton2"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              <img
                width="32"
                height="32"
                src={current?.imageUrl}
                style={styles.img}
                alt="profile"
              />
            </button>
            <ul
              style={styles.dropdown}
              class="dropdown-menu dropdown-menu-dark"
              aria-labelledby="dropdownMenuButton2"
            >
              <li>
                <GoogleLogout
                  clientId={CLIENT_ID}
                  buttonText="Logout"
                  onLogoutSuccess={handleLogoutSuccess}
                  onFailure={handleLogoutFailure}
                />
              </li>
            </ul>
          </div>
        </>
      ) : (
        <GoogleLogin
          clientId={CLIENT_ID}
          buttonText="Login"
          onSuccess={handleLoginSuccess}
          onFailure={handleLoginFailure}
          cookiePolicy={"single_host_origin"}
          responseType="code,token"
          isSignedIn={true}
        />
      )}
    </>
  );
};
export default GoogleBtn;
