export const user = (state = {current: null, error: null}, {type, payload}) => {
    switch (type) {
        case "LOGIN": {
            return payload
        }
        case "LOGOUT": {
            return { current: null, error: null}
        }
        case "ERROR_AUTH": {
            return payload
        }
        case "UPDATE_USER_PROFILE": {
            return {
                ...state,
                current: {
                    ...state.current,
                    ...payload.details
                }
            }
        }
        default:
            return state
}
}

export const handleLogin = user => {
    return {
        type: "LOGIN",
        payload: { current: user, error: null}
    }
}
export const handleLogout = user => {
    return {
        type: "LOGOUT"
    }
}
export const handleErrors = error => {
    return {
        type: "ERROR_AUTH",
        payload: { current: null, error: error}
    }
}

export const updateUserProfile = (details) => {
    return {
        type: "UPDATE_USER_PROFILE",
        payload: {details}
    }
}